import * as firebase from 'firebase';

// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyCTydG3QTWminawbBYMnqMVChyAifYOce0",
    authDomain: "react-native-ade99.firebaseapp.com",
    databaseURL: "https://react-native-ade99.firebaseapp.com",
    projectId: "react-native-ade99",
    storageBucket: "react-native-ade99.appspot.com",
    messagingSenderId: "287936232256",
    appId: "1:287936232256:web:516c89d6783fd1296258a5"
};
const firebaseApp = firebase.initializeApp(firebaseConfig);

export const firebaseAuth = firebaseApp.auth();