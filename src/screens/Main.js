import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dataSource: []
        };
    }

    componentDidMount() {
        fetch("http://jsonplaceholder.typicode.com/posts")
            .then(response => response.json())
            .then((responseJson) => {
                this.setState({
                    dataSource: responseJson
                })
            })
            .catch(error => console.log(error)) //to catch the errors if any
    }

    render() {
        return (
            <View style={{ padding: 10 }}>
                <FlatList
                    padding={10}
                    data={this.state.dataSource}
                    renderItem={({ item }) =>
                        <View>
                            <View style={{ height: 50, flexDirection: 'row', alignContent: 'center', alignItems: 'center' }}>
                                <Text style={{ height: 50, width: '5%' }}>{item.id}</Text>
                                <Text style={{ height: 50 }}>{item.title}</Text>
                            </View>
                            <View style={{ height: 1, backgroundColor: 'gray' }}></View>
                        </View>
                    }
                />

            </View>
        )
    }
}