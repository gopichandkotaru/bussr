import React, { Fragment } from 'react';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import Loading from './src/screens/Loading';
import Login from './src/screens/Login';
import SignUp from './src/screens/SignUp';
import Main from './src/screens/Main';

const SwitchNavigator = createSwitchNavigator(
  {
    Loading: Loading,
    Login: Login,
    SignUp: SignUp,
    Main: Main
  },
  {
    initialRouteName: 'Loading',
    headerMode: 'none'
  }
)

const App = createAppContainer(SwitchNavigator);
export default App